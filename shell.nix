let
  moz_overlay = import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz);
  nixpkgs = import <nixpkgs> { overlays = [ moz_overlay ]; };
  ruststable = ((nixpkgs.rustChannelOf {
    channel = "1.52.1";
  }).rust.override {
    extensions = [ "rust-src" "rust-analysis" "rustfmt-preview" "clippy-preview"];
    targets = ["x86_64-unknown-linux-gnu"];
  });
in
  with nixpkgs;
  stdenv.mkDerivation {
    name = "rust";
    buildInputs = [
      pkgconfig
      ruststable
      latest.rustChannels.stable.cargo
      glib
      jack2
      linuxPackages.perf
      gdb
    ];
  }

