use std::convert::TryFrom;
use std::sync::mpsc::{Receiver, TryRecvError};

use lazy_static::lazy_static;

use crate::cmd::Cmd;
use crate::dag::Dag;
use crate::midi::ControlChange;
use crate::Output;

use crate::nodes::*;

lazy_static! {
    static ref WTSINE: WaveTable = WaveTable::sine();
}

struct Edge {
    from_id: usize,
    from_ix: usize,
    to_ix: usize,
}

pub struct System {
    // id to increment each time a node is added
    id_iter: usize,
    // dag to keep track of order to go over things
    dag: Dag,
    // Remember which order to traverse so we don't have to sort regularly
    traverse: Vec<usize>,
    // All nodes mapped by id
    nodes: Vec<Box<dyn Output + Send>>,
    // buffer for input, should always be the same length as the above node
    // tuple is for (ix, val)
    cc_buffer: Vec<Vec<Option<f32>>>,
    // to id to edge allows us to get all inputs easily
    edges: Vec<Vec<Edge>>,
    // Vector of (id, ix, vol) to sum for output
    outputs: Vec<(usize, usize, f32)>,

    // node id, node ix
    cc_inputs: Vec<Vec<(usize, usize)>>,
}

impl System {
    pub fn new() -> Self {
        System::default()
    }

    pub fn add_node(&mut self, node: Box<dyn Output + Send>) -> usize {
        let id = self.id_iter;
        self.id_iter += 1;
        self.nodes.push(node);
        self.edges.push(Vec::new());
        self.dag.add(id);
        self.cc_buffer.push(Vec::new());
        self.cc_inputs.push(Vec::new());
        self.traverse = self.dag.sort().iter().copied().collect();
        id
    }

    pub fn connect(
        &mut self,
        from_id: usize,
        from_ix: usize,
        to_id: usize,
        to_ix: usize,
    ) {
        self.dag.add_edge(from_id, to_id);
        if let Some(edge) = self.edges.get_mut(to_id) {
            edge.push(Edge {
                from_id,
                from_ix,
                to_ix,
            });
        } else {
            panic!("Should never get here?");
        }
        self.traverse = self.dag.sort().iter().copied().collect();
    }

    pub fn add_output(&mut self, id: usize, ix: usize, vol: f32) {
        self.outputs.push((id, ix, vol));
    }

    pub fn add_cc_input(&mut self, cc_chan: usize, id: usize, ix: usize) {
        if cc_chan >= self.cc_inputs.len() {
            self.cc_inputs.resize(cc_chan + 1, vec![]);
        }
        if ix >= self.cc_buffer[id].len() {
            self.cc_buffer[id].resize(ix + 1, None);
        }
        self.cc_inputs[cc_chan].push((id, ix));
    }

    // process an input
    fn process_cc(&mut self, ix: usize, val: isize) {
        for (node_id, node_ix) in &self.cc_inputs[ix] {
            self.cc_buffer[*node_id][*node_ix] = Some(val as f32);
        }
    }
}

impl Default for System {
    fn default() -> Self {
        System {
            id_iter: 0,
            dag: Dag::new(),
            traverse: Vec::new(),
            cc_buffer: Vec::new(),
            nodes: Vec::new(),
            edges: Vec::new(),
            outputs: Vec::new(),
            cc_inputs: Vec::new(),
        }
    }
}

impl Output for System {
    fn gen(&mut self) {
        for id in &self.traverse {
            let mut v;
            self.nodes[*id].reset_inputs();
            for (ix, val) in self.cc_buffer[*id].iter().enumerate() {
                match *val {
                    Some(val) => self.nodes[*id].add_input(ix, val),
                    None => (),
                }
            }
            for edge in &self.edges[*id] {
                v = self.nodes[edge.from_id].get_output(edge.from_ix);
                self.nodes[*id].add_input(edge.to_ix, v);
            }
            self.nodes[*id].gen()
        }
        for buf in self.cc_buffer.iter_mut() {
            for val in buf.iter_mut() {
                *val = None;
            }
        }
    }

    fn add_input(&mut self, _ix: usize, _val: f32) {}
    fn reset_inputs(&mut self) {}

    fn get_output(&self, _ix: usize) -> f32 {
        let mut v = 0.0;
        for (id, ix, vol) in &self.outputs {
            if let Some(node) = self.nodes.get(*id) {
                v += vol * node.get_output(*ix);
            }
        }
        v
    }
}

pub struct Universe {
    sys: System,
    midi_in: jack::Port<jack::MidiIn>,
    out_l: jack::Port<jack::AudioOut>,
    out_r: jack::Port<jack::AudioOut>,
    cmd_rx: Receiver<Cmd>,
}

impl Universe {
    pub fn new(
        sys: System,
        midi_in: jack::Port<jack::MidiIn>,
        out_l: jack::Port<jack::AudioOut>,
        out_r: jack::Port<jack::AudioOut>,
        cmd_rx: Receiver<Cmd>,
    ) -> Self {
        Universe {
            sys,
            midi_in,
            out_l,
            out_r,
            cmd_rx,
        }
    }

    pub fn clear_system(&mut self) {
        self.sys = System::new();
    }

    fn process_cmd(&mut self, cmd: Cmd) {
        match cmd {
            Cmd::Osc { freq, ratio } => {
                let node = Osc::new(freq, ratio, &WTSINE);
                self.sys.add_node(Box::new(node));
            }
            Cmd::Mult { multiplier } => {
                let node = Multiplier::new(multiplier);
                self.sys.add_node(Box::new(node));
            }
            Cmd::Arp { vals } => {
                let node = Arp::new(vals);
                self.sys.add_node(Box::new(node));
            }
            Cmd::Sqr { on, off } => {
                let node = Square::new(on, off);
                self.sys.add_node(Box::new(node));
            }
            Cmd::ADSR {
                attack,
                decay,
                sustain,
                release,
            } => {
                let node = ADSR::new(attack, decay, sustain, release);
                self.sys.add_node(Box::new(node));
            }
            Cmd::Filter { cutoff } => {
                let node = Filter::new(false, cutoff);
                self.sys.add_node(Box::new(node));
            }
            Cmd::Noise => {
                let node = Noise::new();
                self.sys.add_node(Box::new(node));
            }
            Cmd::Comb { alpha, beta, delay } => {
                let node = Comb::new(alpha, beta, delay);
                self.sys.add_node(Box::new(node));
            }
            Cmd::Reverb => {
                let node = Reverb::new();
                self.sys.add_node(Box::new(node));
            }
            Cmd::Const => {
                let node = Const::default();
                self.sys.add_node(Box::new(node));
            }
            Cmd::Output { id, ix, vol } => self.sys.add_output(id, ix, vol),
            Cmd::Connect {
                from_id,
                from_ix,
                to_id,
                to_ix,
            } => self.sys.connect(from_id, from_ix, to_id, to_ix),
            Cmd::CCInput { chan, id, ix } => {
                self.sys.add_cc_input(chan, id, ix);
            }
            Cmd::Clear => self.clear_system(),
        }
    }
}

impl jack::ProcessHandler for Universe {
    fn process(
        &mut self,
        _: &jack::Client,
        ps: &jack::ProcessScope,
    ) -> jack::Control {
        let out_l = self.out_l.as_mut_slice(ps);
        let out_r = self.out_r.as_mut_slice(ps);
        for msg in self.midi_in.iter(ps) {
            if let Ok(cc) = ControlChange::try_from(msg) {
                if cc.ctrl_chan < self.sys.cc_inputs.len() {
                    self.sys.process_cc(cc.ctrl_chan, cc.ctrl_val);
                } else {
                    println!("No set up channel: {:?}", self.sys.cc_inputs);
                }
            }
        }

        for (l, r) in out_l.iter_mut().zip(out_r.iter_mut()) {
            self.sys.gen();
            *l = self.sys.get_output(0);
            *r = *l;
        }

        // deal with commands
        match self.cmd_rx.try_recv() {
            Ok(cmd) => self.process_cmd(cmd),
            Err(TryRecvError::Empty) => (),
            Err(TryRecvError::Disconnected) => {
                println!("channel disconnected!");
                return jack::Control::Quit;
            }
        }
        jack::Control::Continue
    }
}
