/// simple dag structure to allow us to do a topological traverse, doesn't
/// contain finer detail of where inputs and outputs go.
use std::collections::HashMap;

#[derive(Debug)]
pub struct Dag {
    /// Nodes with no inputs
    pub start_nodes: HashMap<usize, usize>,
    /// nodes with inputs
    nodes: HashMap<usize, usize>,
    /// Edges from id to list of ids
    edges: HashMap<usize, Vec<usize>>,
}

impl Dag {
    pub fn new() -> Dag {
        Dag {
            start_nodes: HashMap::new(),
            nodes: HashMap::new(),
            edges: HashMap::new(),
        }
    }

    pub fn add(&mut self, id: usize) {
        self.start_nodes.insert(id, 0);
    }

    pub fn add_edge(&mut self, from: usize, to: usize) -> bool {
        match self.edges.get_mut(&from) {
            None => {
                self.edges.insert(from, vec![to]);
                self.add_input(to);
                true
            }
            Some(es) => {
                if es.contains(&to) {
                    false
                } else {
                    es.push(to);
                    self.add_input(to);
                    true
                }
            }
        }
    }

    fn add_input(&mut self, to: usize) {
        // Increase input count of node and move from start_nodes to nodes if necessary.
        match self.start_nodes.remove(&to) {
            None => match self.nodes.get_mut(&to) {
                Some(v) => {
                    *v += 1;
                }
                None => panic!("value not in nodes!"),
            },
            Some(mut v) => {
                v += 1;
                self.nodes.insert(to, v);
            }
        };
    }

    pub fn sort(&self) -> Vec<usize> {
        // This uses kahn's algorithm to give us a vector ids
        let mut sorted = Vec::new();
        let mut edges = self.edges.clone();

        // Get start nodes to begin with
        let mut incoming: Vec<usize> =
            self.start_nodes.keys().copied().collect();
        let mut rest: HashMap<usize, usize> = self.nodes.clone();

        // Only in incoming if it's in start_nodes or count is now 0
        while let Some(id) = incoming.pop() {
            sorted.push(id);

            // Go through edges and decrement count on to nodes, adding to input
            // if count is now 0
            let ms = match edges.remove(&id) {
                Some(x) => x,
                None => continue,
            };
            for m in ms {
                let count = match rest.get_mut(&m) {
                    Some(count) => count,
                    None => panic!("Kahn was wrong!"),
                };
                *count -= 1;
                if *count == 0 {
                    incoming.push(m);
                    rest.remove(&m);
                }
            }
        }

        if !incoming.is_empty() || !rest.is_empty() {
            println!("{:?}", incoming);
            println!("{:?}", rest);
            panic!("Incoming nodes still remain, is there a loop?");
        }

        sorted
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn all_start_nodes() {
        let mut dag = Dag::new();
        dag.add(0);
        dag.add(1);
        dag.add(2);
        dag.sort();
    }

    #[test]
    fn one_start_node() {
        let mut dag = Dag::new();
        dag.add(0);
        dag.add(1);
        dag.add(2);
        dag.add(3);
        dag.add_edge(0, 1);
        dag.add_edge(0, 2);
        dag.add_edge(0, 3);
        assert_eq!(dag.sort().iter().next().copied(), Some(0usize));
    }

    #[test]
    fn line() {
        // only one possible order for this
        let mut dag = Dag::new();
        dag.add(0);
        dag.add(1);
        dag.add(2);
        dag.add(3);
        dag.add_edge(0, 1);
        dag.add_edge(1, 2);
        dag.add_edge(2, 3);
        assert_eq!(dag.sort(), vec![0, 1, 2, 3]);
    }

    #[test]
    #[should_panic]
    fn cycle() {
        let mut dag = Dag::new();
        dag.add(0);
        dag.add(1);
        dag.add(2);
        dag.add(3);
        dag.add_edge(0, 1);
        dag.add_edge(1, 2);
        dag.add_edge(2, 3);
        dag.add_edge(3, 0);
        dag.sort();
    }
}
