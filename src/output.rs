pub trait Output {
    fn gen(&mut self);
    fn get_output(&self, ix: usize) -> f32;
    fn add_input(&mut self, ix: usize, val: f32);
    fn reset_inputs(&mut self);
}
