use std::str::FromStr;

#[derive(Debug)]
pub enum Cmd {
    // Node commands
    Osc {
        freq: f32,
        ratio: f32,
    },
    Mult {
        multiplier: f32,
    },
    Arp {
        vals: Vec<f32>,
    },
    Sqr {
        on: f32,
        off: f32,
    },
    ADSR {
        attack: f32,
        decay: f32,
        sustain: f32,
        release: f32,
    },
    Filter {
        cutoff: f32,
    },
    Noise,
    Comb {
        alpha: f32,
        beta: f32,
        delay: usize,
    },
    Reverb,
    Const,

    // system commands
    Connect {
        from_id: usize,
        from_ix: usize,
        to_id: usize,
        to_ix: usize,
    },

    // universe commands
    Output {
        id: usize,
        ix: usize,
        vol: f32,
    },
    CCInput {
        chan: usize,
        id: usize,
        ix: usize,
    },

    Clear,
}

pub enum CmdErr {
    Unknown,
    Empty,
    Comment,
    MissingField,
    ParseFailure,
}

fn next_and_parse<'a, I, R>(iter: &mut I) -> Result<R, CmdErr>
where
    I: Iterator<Item = &'a str>,
    R: FromStr,
{
    iter.next()
        .ok_or(CmdErr::MissingField)?
        .parse()
        .map_err(|_| CmdErr::ParseFailure)
}

// This is a potentially temporary solution, as we could use cbor or something
// later on to send messages, though this might not be an issue
impl FromStr for Cmd {
    type Err = CmdErr;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with('#') {
            return Err(CmdErr::Comment);
        }
        let mut word_iter = s.split_whitespace();
        match word_iter.next() {
            Some("osc") => {
                let freq = next_and_parse(&mut word_iter)?;
                let ratio = next_and_parse(&mut word_iter)?;
                Ok(Cmd::Osc { freq, ratio })
            }
            Some("output") => {
                let id = next_and_parse(&mut word_iter)?;
                let ix = next_and_parse(&mut word_iter)?;
                let vol = next_and_parse(&mut word_iter)?;
                Ok(Cmd::Output { id, ix, vol })
            }
            Some("connect") => {
                let from_id = next_and_parse(&mut word_iter)?;
                let from_ix = next_and_parse(&mut word_iter)?;
                let to_id = next_and_parse(&mut word_iter)?;
                let to_ix = next_and_parse(&mut word_iter)?;
                Ok(Cmd::Connect {
                    from_id,
                    from_ix,
                    to_id,
                    to_ix,
                })
            }
            Some("cc") => {
                let chan = next_and_parse(&mut word_iter)?;
                let id = next_and_parse(&mut word_iter)?;
                let ix = next_and_parse(&mut word_iter)?;
                Ok(Cmd::CCInput { chan, id, ix })
            }
            Some("mult") => {
                let multiplier = next_and_parse(&mut word_iter)?;
                Ok(Cmd::Mult { multiplier })
            }
            Some("arp") => {
                let mut vals = Vec::new();
                while let Some(val) = word_iter.next() {
                    vals.push(val.parse().map_err(|_| CmdErr::ParseFailure)?);
                }
                Ok(Cmd::Arp { vals })
            }
            Some("sqr") => {
                let on = next_and_parse(&mut word_iter)?;
                let off = next_and_parse(&mut word_iter)?;
                Ok(Cmd::Sqr { on, off })
            }
            Some("adsr") => {
                let attack = next_and_parse(&mut word_iter)?;
                let decay = next_and_parse(&mut word_iter)?;
                let sustain = next_and_parse(&mut word_iter)?;
                let release = next_and_parse(&mut word_iter)?;
                Ok(Cmd::ADSR {
                    attack,
                    decay,
                    sustain,
                    release,
                })
            }
            Some("filter") => {
                let cutoff = next_and_parse(&mut word_iter)?;
                Ok(Cmd::Filter { cutoff })
            }
            Some("noise") => Ok(Cmd::Noise),
            Some("const") => Ok(Cmd::Const),
            Some("clear") => Ok(Cmd::Clear),
            Some("comb") => {
                let alpha = next_and_parse(&mut word_iter)?;
                let beta = next_and_parse(&mut word_iter)?;
                let delay = next_and_parse(&mut word_iter)?;
                Ok(Cmd::Comb { alpha, beta, delay })
            }
            Some("reverb") => Ok(Cmd::Reverb),
            Some(_) => Err(CmdErr::Unknown),
            None => Err(CmdErr::Empty),
        }
    }
}
