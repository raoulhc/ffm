// Module to process raw midi
// This will initially just be cc

use jack::RawMidi;
use std::convert::TryFrom;

#[derive(Debug)]
pub struct ControlChange {
    pub chan: usize,
    pub ctrl_chan: usize,
    // We use 60 as 0 to give us signed values
    pub ctrl_val: isize,
}

impl<'a> TryFrom<RawMidi<'a>> for ControlChange {
    type Error = &'static str;

    fn try_from(midi: RawMidi) -> Result<Self, Self::Error> {
        match midi.bytes[0] & 0b1111_0000 {
            176 => (),
            _ => return Err("Non control change msg"),
        }
        if midi.bytes.len() != 3 {
            return Err("Unexpected byte length");
        }
        Ok(ControlChange {
            chan: (midi.bytes[0] & 0b0000_1111) as usize,
            ctrl_chan: midi.bytes[1] as usize,
            ctrl_val: midi.bytes[2] as isize - 60,
        })
    }
}
