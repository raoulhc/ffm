use crate::consts::TIME_STEP;
use crate::output::Output;

pub struct ADSR {
    // parameters
    attack: f32,
    decay: f32,
    sustain: f32,
    release: f32,
    // triggers
    trigger_on: f32,
    trigger_off: f32,

    // internal variables
    input: f32,
    output: f32,
    step: f32,
    on: bool,

    // some bools for debugging output
    attack_dbg: bool,
    decay_dbg: bool,
    sustain_dbg: bool,
    release_dbg: bool,
}

impl ADSR {
    pub fn new(attack: f32, decay: f32, sustain: f32, release: f32) -> Self {
        ADSR {
            attack,
            decay,
            sustain,
            release,
            trigger_on: 0.99,
            trigger_off: 0.99,
            input: 0.0,
            output: 0.0,
            step: 0.0,
            on: false,
            attack_dbg: false,
            decay_dbg: false,
            sustain_dbg: false,
            release_dbg: false,
        }
    }

    pub fn triggers(&mut self, trigger_on: f32, trigger_off: f32) {
        self.trigger_on = trigger_on;
        self.trigger_off = trigger_off;
    }
}

impl Output for ADSR {
    fn gen(&mut self) {
        // See if we've been triggered
        if !self.on {
            if self.input >= self.trigger_on {
                self.step = 0.0;
                self.output = 0.0;
                self.on = true;
            } else {
                // This prevents it dropping away too quickly at the end and
                // giving us pops.
                self.output *= 0.9;
                return;
            }
        }

        // Go through attack decay sustain release phases
        if self.step < self.attack {
            self.output += TIME_STEP / self.attack;
            self.step += TIME_STEP;
            if !self.attack_dbg {
                // println!("attack!");
                self.attack_dbg = true;
            }
        } else if self.step < self.attack + self.decay {
            self.output -= (1.0 - self.sustain) * TIME_STEP / self.decay;
            self.step += TIME_STEP;
            if !self.decay_dbg {
                // println!("decay!");
                self.decay_dbg = true;
            }
        } else if self.step >= self.attack + self.decay
            && self.input >= self.trigger_off
        {
            if !self.sustain_dbg {
                // println!("sustain!");
                self.sustain_dbg = true;
            }
        } else if self.step < self.attack + self.decay + self.release {
            self.output -= TIME_STEP / self.release;
            self.step += TIME_STEP;
            if !self.release_dbg {
                // println!("release!");
                self.release_dbg = true;
            }
        } else {
            self.on = false;
            self.step = 0.0;
            self.attack_dbg = false;
            self.decay_dbg = false;
            self.sustain_dbg = false;
            self.release_dbg = false;
        }
    }

    fn get_output(&self, _ix: usize) -> f32 {
        self.output
    }
    fn add_input(&mut self, _ix: usize, val: f32) {
        self.input += val;
    }

    fn reset_inputs(&mut self) {
        self.input = 0.0;
    }
}
