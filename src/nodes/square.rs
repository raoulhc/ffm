use crate::consts::TIME_STEP;
use crate::output::Output;

// Structure for generating square waves useful for triggering other nodes
// but can also just be used as output
pub struct Square {
    on: f32,
    off: f32,
    on_bool: bool,
    step: f32,
}

impl Square {
    pub fn new(on: f32, off: f32) -> Self {
        Square {
            on,
            off,
            on_bool: false,
            step: 0.0,
        }
    }
}

impl Output for Square {
    fn gen(&mut self) {
        self.step += TIME_STEP;
        if self.on_bool && self.step >= self.on {
            self.step %= self.on;
            self.on_bool = false;
        } else if !self.on_bool && self.step >= self.off {
            self.step %= self.off;
            self.on_bool = true;
        }
    }

    fn get_output(&self, _ix: usize) -> f32 {
        if self.on_bool {
            1.0
        } else {
            0.0
        }
    }
    fn add_input(&mut self, ix: usize, val: f32) {
        match ix {
            0 => {
                self.on += 0.1 * val;
                println!("new on time {}", self.on);
            }
            1 => {
                self.off += 0.1 * val;
                println!("new off time {}", self.off);
            }
            _ => panic!("Invalid input"),
        }
    }
    fn reset_inputs(&mut self) {}
}
