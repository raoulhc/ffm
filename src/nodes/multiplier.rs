use crate::output::Output;

pub struct Multiplier {
    input: f32,
    output: f32,
    multiplier: f32,
    base: f32,
}

impl Multiplier {
    pub fn new(multiplier: f32) -> Self {
        Multiplier {
            input: 0.0,
            output: 0.0,
            multiplier,
            base: 0.0,
        }
    }

    pub fn base(&mut self, base: f32) {
        self.base = base;
    }

    pub fn mult_multiplier(&mut self, mult: f32) {
        self.multiplier *= 1. + 0.1 * mult;
    }
}

impl Output for Multiplier {
    fn gen(&mut self) {
        self.output = (self.base + self.input) * self.multiplier
    }

    fn get_output(&self, _ix: usize) -> f32 {
        self.output
    }

    fn add_input(&mut self, ix: usize, val: f32) {
        match ix {
            0 => self.input += val,
            1 => self.multiplier = val,
            2 => self.multiplier += val,
            3 => self.mult_multiplier(val),
            _ => panic!("Unsupported index {} for multiplier", ix),
        }
    }

    fn reset_inputs(&mut self) {
        self.input = 0.0;
    }
}
