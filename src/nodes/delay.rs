use crate::Output;

pub struct Delay {
    input: f32,
    output: f32,
    delay: usize,
    index: usize,
    // We need to box this else we might get a stack overflow on the jack thread
    buffer: Box<[f32; 48000]>,
}

impl Delay {
    pub fn new(delay: usize) -> Self {
        Delay {
            input: 0.0,
            output: 0.0,
            delay,
            index: 0,
            buffer: Box::new([0.0; 48000]),
        }
    }
}

impl Output for Delay {
    fn gen(&mut self) {
        self.buffer[(self.index + self.delay) % self.delay] = self.input;
        self.output = self.buffer[self.index];
        self.index += 1;
        self.index %= self.delay;
    }
    fn reset_inputs(&mut self) {
        self.input = 0.0;
    }
    fn get_output(&self, _ix: usize) -> f32 {
        self.output
    }
    fn add_input(&mut self, _ix: usize, val: f32) {
        self.input = val;
    }
}
