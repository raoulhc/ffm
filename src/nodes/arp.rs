use crate::output::Output;

// Each time input triggers it'll move to the next value
pub struct Arp {
    vals: Vec<f32>,
    ix: usize,
    input: f32,
    output: f32,
    trigger_on: f32,
    trigger_off: f32,
    triggered: bool,
}

impl Arp {
    pub fn new(vals: Vec<f32>) -> Self {
        Arp {
            vals,
            ix: usize::MAX - 1,
            input: 0.0,
            output: 0.0,
            trigger_on: 0.99,
            trigger_off: 0.01,
            triggered: false,
        }
    }

    pub fn add(&mut self, val: f32) {
        self.vals.push(val);
    }
}

impl Output for Arp {
    fn gen(&mut self) {
        if !self.triggered && self.input >= self.trigger_on {
            self.triggered = true;
            self.ix += 1;
            if self.ix >= self.vals.len() {
                self.ix = 0;
            }
            self.output = self.vals[self.ix];
        } else if self.triggered && self.input < self.trigger_off {
            self.triggered = false;
        }
    }
    fn add_input(&mut self, _ix: usize, val: f32) {
        self.input = val;
    }
    fn get_output(&self, _ix: usize) -> f32 {
        self.output
    }
    fn reset_inputs(&mut self) {
        self.input = 0.0;
    }
}
