use rand::prelude::*;
use rand_chacha::ChaCha20Rng;

use crate::output::Output;

pub struct Noise {
    rng: ChaCha20Rng,
    output: f32,
}

impl Noise {
    pub fn new() -> Self {
        Noise {
            rng: ChaCha20Rng::from_entropy(),
            output: 0.0,
        }
    }
}

impl Output for Noise {
    fn gen(&mut self) {
        self.output = self.rng.gen_range(-1.0..1.0);
    }
    fn get_output(&self, _ix: usize) -> f32 {
        self.output
    }

    fn add_input(&mut self, _ix: usize, _val: f32) {}

    fn reset_inputs(&mut self) {}
}
