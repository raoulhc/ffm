use crate::Output;
use std::f32::consts::SQRT_2;
// use crate::consts::{TIME_STEP, TAU, PHASE_STEP};
use crate::consts::TAU;

// Let's have a go writing a bilinear transformed biquad section second order butterworth filter
// using direct form 2.
//
// The transfer function for a second order butterworth filter is
//                  1
//   H(s) = -------------------
//          1 + sqrt(2) s + s^2
//
// And the direct form two is these simple difference equations
//
// y[n] = b_0 w[n] + b_1 w[n-1] + b_2 w[n-2]
// w[n] = x[n] - a_1 w[n-1] - a_2 w[n-2]
//
// the original butterworth constants in terms of a cutoff frequency
// are
//   a_0 = 1
//   a_1 = sqrt(2) * K/w_c
//   a_2 = (K/w_c)^2
//   b_0 = 1
//   b_1 = 0
//   b_2 = 0
//
// where w_c is the angular cutoff frequency.
//
// To get this to work for discrete time we do a bilinear transform and these constants become:
//
//   b_0 = w_c^2 / den
//   b_1 = + 2 * w_c^2 / den
//   b_2 = w_c^2 / dem
//   a_1 = (2 * w_c^2 - 2 K^2) / den
//   a_2 = (K^2 - sqrt(2) K / w_c + 1/w_c^2) / den
// where
//   den = K^2 + sqrt(2) * K * w_c + 1 * w_c^2
//   K = 2 / T
//
// To get the inverse high pass filter we need to transform the transfer function by replacing s
// with (1/s). This swaps the values of b_0 and b_2 (b_0=0 and b_2=1 now) in the original transfer
// function, and the new transformed b constants become:
//
//   b_0 = K^2 / dem
//   b_1 = - 2 K^2 / dem
//   b_2 = K^2 / dem
//
// The a constants and denominator remain the same.

// This constant is to gradually move the filter cutoff to by multiplying the value per sample
// towards it
const CUTOFF_SMOOTHING: f32 = 1.0005;

pub struct Filter {
    cutoff: f32,
    high: bool, // Whether the filter is in high pass or low pass mode
    // this is used to smooth out changing of the cutoff
    cutoff_var: f32,
    // butterworth constants
    b_0: f32,
    b_1: f32,
    b_2: f32,
    a_1: f32,
    a_2: f32,
    // delay lines needed
    delay_0: f32,
    delay_1: f32,
    delay_2: f32,
    // io variables
    input: f32,
    output: f32,
}

impl Default for Filter {
    fn default() -> Self {
        Filter {
            cutoff: 0.0,
            cutoff_var: 1.0,
            high: false,
            b_0: 0.0,
            b_1: 0.0,
            b_2: 0.0,
            a_1: 0.0,
            a_2: 0.0,
            delay_0: 0.0,
            delay_1: 0.0,
            delay_2: 0.0,
            input: 0.0,
            output: 0.0,
        }
    }
}

impl Filter {
    pub fn new(high: bool, cutoff: f32) -> Self {
        let mut x = Filter::default();
        x.high = high;
        x.cutoff(cutoff);
        x
    }
    // This calculates all the constants used
    fn cutoff(&mut self, cutoff: f32) {
        self.cutoff = cutoff;
    }

    // Calculate new cutoff if needed, smoothing to avoid significant discontinuities.
    fn cutoff_gen(&mut self) {
        // Fine to compare like this as it will be set directly
        #[allow(clippy::float_cmp)]
        if self.cutoff_var != self.cutoff {
            if self.cutoff_var > self.cutoff * CUTOFF_SMOOTHING {
                self.cutoff_var /= CUTOFF_SMOOTHING;
            } else if self.cutoff_var < self.cutoff / CUTOFF_SMOOTHING {
                self.cutoff_var *= CUTOFF_SMOOTHING;
            } else {
                self.cutoff_var = self.cutoff
            }
            let w_c = TAU * self.cutoff_var / 48_000.0;
            let w_c_2 = w_c.powf(2.0);
            let k = 2.0;
            let k_2 = 4.0;
            let dem = k_2 + (k * SQRT_2 * w_c) + w_c_2;
            self.b_0 = if self.high { k_2 } else { w_c_2 } / dem;
            self.b_1 = 2.0 * if self.high { w_c_2 } else { -k_2 } / dem;
            self.b_2 = if self.high { k_2 } else { w_c_2 } / dem;
            self.a_1 = (2.0 * w_c_2 - 2.0 * k_2) / dem;
            self.a_2 = (k_2 - (k * SQRT_2 * w_c) + w_c_2) / dem;
        }
    }
}

impl Output for Filter {
    // This does direct form 2
    fn gen(&mut self) {
        // generate newe cutoff value
        self.cutoff_gen();
        // calculate w_n given x[n]
        self.delay_0 =
            self.input - self.a_1 * self.delay_1 - self.a_2 * self.delay_2;
        // calcuate y[n] with w[n] w[n-1] and w[n-2]
        self.output = self.b_0 * self.delay_0
            + self.b_1 * self.delay_1
            + self.b_2 * self.delay_2;
    }

    // resets and pushses back delay lines
    fn reset_inputs(&mut self) {
        self.delay_2 = self.delay_1;
        self.delay_1 = self.delay_0;
        self.input = 0.0;
    }

    fn get_output(&self, _ix: usize) -> f32 {
        self.output
    }

    fn add_input(&mut self, ix: usize, val: f32) {
        match ix {
            0 => self.input += val,
            1 => self.cutoff(self.cutoff * (1. + 0.05 * val)),
            _ => (),
        }
    }
}
