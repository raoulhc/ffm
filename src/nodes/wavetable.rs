use crate::consts::*;

// Only need one, should be shared between oscillators.
#[derive(Debug)]
pub struct WaveTable {
    pub arr: [f32; 512],
}

impl WaveTable {
    const SIZE: f32 = 512.0;

    // Set up a wave table
    pub fn sine() -> Self {
        let mut arr = [0.0; 512];
        for (ix, val) in arr.iter_mut().enumerate() {
            *val = (ix as f32 * TAU / (WaveTable::SIZE - 1.)).sin();
        }
        WaveTable { arr }
    }

    pub fn saw() -> Self {
        let mut arr = [0.0; Self::SIZE as usize];
        let mut v = 0.0;
        let step = 2.0 / Self::SIZE;
        for val in arr.iter_mut() {
            *val = v;
            v += step;
            if v >= 1.0 {
                v -= 2.0
            }
        }
        WaveTable { arr }
    }

    // Given a phase give the sin value at that point
    pub fn phase(&self, phase: f32) -> f32 {
        // get before and after points
        let ixf = phase * (WaveTable::SIZE - 1.) / TAU;
        let x1 = ixf.floor();
        let x2 = ixf.ceil();
        let y1 = self.arr[x1 as usize];
        let y2 = self.arr[x2 as usize];

        // linear interp
        y1 + (y2 - y1) * (ixf - x1)
    }
}
