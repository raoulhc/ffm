use crate::Output;

pub struct Const {
    output: f32,
    range_l: f32,
    range_h: f32,
    mult: f32,
}

impl Const {
    pub fn default() -> Self {
        Const {
            output: 0.0,
            range_l: 0.0,
            range_h: 1.0,
            mult: 0.01,
        }
    }

    pub fn new(range_l: f32, range_h: f32, mult: f32) -> Const {
        Const {
            output: 0.0,
            range_l,
            range_h,
            mult,
        }
    }
}

impl Output for Const {
    fn gen(&mut self) {}

    fn get_output(&self, _ix: usize) -> f32 {
        self.output
    }

    fn add_input(&mut self, _ix: usize, val: f32) {
        println!("val: {}, {}", val, self.output);
        self.output += self.mult * val;
        if self.output >= self.range_h {
            self.output = self.range_h;
        } else if self.output <= self.range_l {
            self.output = self.range_l;
        }
    }
    fn reset_inputs(&mut self) {}
}
