use crate::nodes::Comb;
use crate::nodes::Delay;
use crate::Output;

struct ReverbUnit {
    comb1: Comb,
    comb2: Comb,
    delay: Delay,
}

impl ReverbUnit {
    fn new(comb1: usize, comb2: usize, delay: usize) -> Self {
        ReverbUnit {
            comb1: Comb::new(0.5, -0.5, comb1),
            comb2: Comb::new(0.5, -0.5, comb2),
            delay: Delay::new(delay),
        }
    }

    fn gen(&mut self, input: f32) -> f32 {
        self.comb1.reset_inputs();
        self.comb1.add_input(0, input);
        self.comb1.gen();
        self.comb2.reset_inputs();
        self.comb2.add_input(0, self.comb1.get_output(0));
        self.comb2.gen();
        self.delay.reset_inputs();
        self.delay.add_input(0, self.comb2.get_output(0));
        self.delay.gen();
        self.delay.get_output(0)
    }
}

pub struct Reverb {
    input: f32,
    output: f32,
    delay: f32,
    tapped_delay: [Delay; 4],
    rev: [ReverbUnit; 6],
}

impl Reverb {
    pub fn new() -> Self {
        Reverb {
            input: 0.0,
            output: 0.0,
            delay: 0.0,
            tapped_delay: [
                Delay::new(4000),
                Delay::new(5000),
                Delay::new(6000),
                Delay::new(8000),
            ],
            rev: [
                ReverbUnit::new(4050, 5500, 3000),
                ReverbUnit::new(2200, 3300, 3600),
                ReverbUnit::new(5100, 2600, 8060),
                ReverbUnit::new(9000, 4300, 2500),
                ReverbUnit::new(8000, 9000, 7500),
                ReverbUnit::new(6666, 6100, 1100),
            ],
        }
    }
}

impl Output for Reverb {
    fn gen(&mut self) {
        // run delay lines
        let mut x = self.input;
        let mut delay_out = 0.2 * self.input;
        for i in 0..4 {
            self.tapped_delay[i].add_input(0, x);
            self.tapped_delay[i].gen();
            x = self.tapped_delay[i].get_output(0);
            delay_out += 0.2 * x;
        }
        // run reverb units
        let out1 = self.rev[0].gen(delay_out);
        let out2 = self.rev[1].gen(out1);
        let out3 = self.rev[2].gen(out2);
        let out4 = self.rev[3].gen(out3);
        let out5 = self.rev[4].gen(out4);
        let out6 = self.rev[5].gen(out5);
        // get output and feed back in
        self.output = (1.0 / 6.0) * (out1 + out2 + out3 + out4 + out5 + out6);
        self.delay = 0.50 * self.output;
    }
    fn reset_inputs(&mut self) {
        self.input = self.delay;
    }
    fn add_input(&mut self, _ix: usize, val: f32) {
        self.input += val;
    }
    fn get_output(&self, _ix: usize) -> f32 {
        self.output
    }
}
