use crate::Output;

// Comb filters are these simple delays with feedforward and feedback coefficients

pub struct Comb {
    input: f32,
    alpha: f32, // Feedback coefficient
    beta: f32,  // Feedforward coefficient
    delay: usize,
    index: usize,
    // We need to box this else we might get stack overflows
    buffer: Box<[f32; 48_000]>, // Up to a second of buffer
    output: f32,
}

impl Comb {
    pub fn new(alpha: f32, beta: f32, delay: usize) -> Self {
        Comb {
            input: 0.0,
            alpha,
            beta,
            delay,
            index: 0,
            buffer: Box::new([0.0; 48_000]),
            output: 0.0,
        }
    }
}

impl Output for Comb {
    fn gen(&mut self) {
        self.input += self.alpha * self.buffer[self.index];
        self.buffer[(self.index + self.delay) % self.delay] = self.input;
        self.output = self.buffer[self.index] + self.beta * self.input;
        self.index += 1;
        self.index %= self.delay;
    }

    fn reset_inputs(&mut self) {
        self.input = 0.0;
    }

    fn add_input(&mut self, ix: usize, val: f32) {
        match ix {
            0 => self.input += val,
            1 => self.alpha += 0.01 * val,
            2 => self.beta += 0.01 * val,
            3 => {
                if -10.0 * val > self.delay as f32 {
                    self.delay = 0;
                } else {
                    if val > 0.0 {
                        self.delay += 10 * val as usize;
                    } else {
                        self.delay -= 10 * val.abs() as usize;
                    }
                    if val.abs() >= 0.1 {
                        println!("delay {}", self.delay);
                    }
                }
            }
            _ => panic!("Inalid input!"),
        }
    }
    fn get_output(&self, _ix: usize) -> f32 {
        self.output
    }
}
