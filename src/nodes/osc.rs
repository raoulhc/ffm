use crate::consts::*;
use crate::nodes::WaveTable;
use crate::output::Output;

pub struct Osc<'a> {
    freq: f32,
    ratio: f32,
    phase: f32,
    input: f32,
    output: f32,
    wave_table: &'a WaveTable,
}

impl<'a> Osc<'a> {
    pub fn new(freq: f32, ratio: f32, wave_table: &'a WaveTable) -> Self {
        Osc {
            freq,
            ratio,
            phase: 0.0,
            input: 0.0,
            output: 0.0,
            wave_table,
        }
    }

    pub fn freq(&mut self, freq: f32) {
        self.freq = freq;
    }
}

impl<'a> Output for Osc<'a> {
    fn gen(&mut self) {
        let res = self.wave_table.phase(self.phase);
        self.phase += (self.freq * self.ratio + self.input) * PHASE_STEP;
        self.phase %= TAU;
        if self.phase < 0.0 {
            self.phase += TAU;
        }
        self.output = res;
    }

    fn get_output(&self, _ix: usize) -> f32 {
        self.output
    }

    fn add_input(&mut self, ix: usize, val: f32) {
        match ix {
            0 => self.input += val,
            1 => self.freq(val),
            2 => {
                if val != 0.0 {
                    self.freq += val;
                }
            }
            _ => panic!("Unsupported index {} for osc", ix),
        }
    }

    fn reset_inputs(&mut self) {
        self.input = 0.0;
    }
}
