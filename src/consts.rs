use std::f32::consts::PI;

pub const TAU: f32 = 2.0 * PI;

pub const PHASE_STEP: f32 = TAU / 48000.;

pub const TIME_STEP: f32 = 1.0 / 48000.0;
