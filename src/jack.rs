use std::fs;
use std::io::{self, BufRead, Read};
use std::os::unix::net::UnixListener;
use std::path::Path;
use std::sync::mpsc::{channel, Sender};

use inotify::{EventMask, Inotify, WatchMask};

use crate::cmd::{Cmd, CmdErr};
use crate::sys::{System, Universe};

fn play<F>(action: F)
where
    F: FnOnce(Sender<Cmd>),
{
    let (client, _st) =
        jack::Client::new("ffm", jack::ClientOptions::NO_START_SERVER).unwrap();
    let out_l = client.register_port("out", jack::AudioOut).unwrap();
    let out_r = client.register_port("out_r", jack::AudioOut).unwrap();
    let midi_in = client.register_port("midi_in", jack::MidiIn).unwrap();
    let (tx, rx) = channel();
    let universe = Universe::new(System::new(), midi_in, out_l, out_r, rx);
    let active_client = client.activate_async((), universe).unwrap();
    action(tx);
    active_client.deactivate().unwrap();
}

fn parse_line(tx: &Sender<Cmd>, line: &str) {
    println!("{}", line);
    match line.parse() {
        Ok(cmd) => {
            let _ = tx.send(cmd);
        }
        Err(CmdErr::Comment) => (),
        Err(CmdErr::Empty) => (),
        Err(_) => println!("Couldn't parse command: {}", line),
    }
}

pub fn play_stdin() {
    play(|tx: Sender<Cmd>| {
        // Wait for user input to quit
        let stdin = io::stdin();
        let mut lines = stdin.lock().lines().peekable();
        let _ = lines.peek();
        for line in &mut lines {
            parse_line(&tx, &line.expect("Couldn't read line!"));
        }

        println!("Press enter/return to quit...");
        let mut line = String::new();
        let res = io::stdin().read_line(&mut line);
        println!("line: {:?}", line);
        println!("res: {:?}", res);
    });
}

pub fn play_socket<F: AsRef<Path>>(path: F) {
    let stream = UnixListener::bind(&path).unwrap();
    let mut response = String::new();
    play(|tx: Sender<Cmd>| loop {
        match stream.accept() {
            Ok((mut socket, addr)) => {
                println!("{:?}", addr);
                response = String::new();
                socket.read_to_string(&mut response).unwrap();
                for line in response.lines() {
                    parse_line(&tx, &line);
                }
            }
            Err(err) => println!("{}", err),
        }
    });
    fs::remove_file(path).unwrap();
}

pub fn play_file<F: AsRef<Path>>(path: F) {
    let mut inotify = Inotify::init().expect("Couldn't initialise inotify");
    inotify
        .add_watch(
            path.as_ref().parent().unwrap(),
            WatchMask::MODIFY | WatchMask::CREATE | WatchMask::DELETE,
        )
        .expect("Failed to add file watch");
    let mut buffer = [0; 1024];
    play(|tx: Sender<Cmd>| {
        // read in file on start up
        for line in fs::read_to_string(path.as_ref()).unwrap().lines() {
            parse_line(&tx, line);
        }
        loop {
            // Don't actually care about the event?
            let events = inotify
                .read_events_blocking(&mut buffer)
                .expect("Couldn't read event stream");
            for event in events {
                println!("event {:?}", event);
                if event.mask.contains(EventMask::MODIFY) {
                    let _ = tx.send(Cmd::Clear);
                    for line in
                        fs::read_to_string(path.as_ref()).unwrap().lines()
                    {
                        parse_line(&tx, line)
                    }
                }
            }
        }
    })
}
