use ffm::play_file;
use std::env;

fn main() {
    if let Some(file) = env::args().nth(1) {
        println!("{}", file);
        play_file(file);
    } else {
        println!("You need to pass a file");
    }
}
